function createNewUser (){

    const firstName = prompt ('Your name: ')
    const lastName = prompt ('Your surname:')
    let userBirth;
    do {  
    userBirth=prompt ('Your birth date:', 'dd.mm.yyyy')
    } while (!/\d{2}.\d{2}.\d{4}/.test(userBirth))

    return {
        firstName,
        lastName,
        userBirth,
        getAge (){
            let currentYear = new Date().getFullYear()
            let currentMonth = new Date().getMonth()
            let currentDay = new Date().getDate()
            const birthDay = userBirth.slice(0,2)
            const birthMonth = userBirth.slice(3,5)
            const birthYear = userBirth.slice(-4)
            let age = currentYear - birthYear
            if (birthMonth > currentMonth || (birthMonth === currentMonth && birthDay > currentDay)){
                age--
            }
            
            return age
        },
        getPassword (){
            return(this.firstName[0]+(this.lastName.toLowerCase())+this.userBirth.slice(-4))
        }
    }
}

let user= createNewUser()
console.log(user.firstName+' '+user.lastName)
console.log(user.getAge())
console.log(user.getPassword())


